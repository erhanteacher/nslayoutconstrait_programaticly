//
//  ViewController.swift
//  hggh
//
//  Created by erhan demirci on 17.09.2019.
//  Copyright © 2019 erhan demirci. All rights reserved.
//w

import UIKit

class ViewController: UIViewController {
    
   
     let myview = UIView()
     let myview2 = UIView()
     let myview3 = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
        let newView = UIView()
        newView.backgroundColor = UIColor.red
        view.addSubview(newView)
        
        newView.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100)
        let heightConstraint = NSLayoutConstraint(item: newView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 100)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        let newView2 = UIView()
        newView2.backgroundColor = UIColor.blue
        view.addSubview(newView2)
        */
        /*newView2.translatesAutoresizingMaskIntoConstraints=false
        let hConstrait=NSLayoutConstraint(item: newView2, attribute: .top, relatedBy:.equal, toItem: newView, attribute: .top, multiplier: 1, constant: 10)
        let vConsstrait=NSLayoutConstraint(item: newView2, attribute: .leading, relatedBy: .equal, toItem: newView, attribute: .leading, multiplier: 1, constant: 10)
        let tralingConsrait=NSLayoutConstraint(item: newView2, attribute: .trailing, relatedBy: .equal, toItem: newView, attribute: .trailing, multiplier: 1, constant: 10)
        let wConstrait=NSLayoutConstraint(item: newView2, attribute: .width, relatedBy: .equal, toItem: newView, attribute: .width, multiplier: 0.5, constant: 0)
        let heConstrait=NSLayoutConstraint(item: newView2, attribute: .height, relatedBy: .equal, toItem: newView, attribute: .width, multiplier: 0.5, constant: 0)
        NSLayoutConstraint.activate([hConstrait,vConsstrait,wConstrait,heConstrait])*/
        
       /* newView2.translatesAutoresizingMaskIntoConstraints=false
        let hConstrait=NSLayoutConstraint(item: newView2, attribute: .top, relatedBy:.equal, toItem: newView, attribute: .top, multiplier: 1, constant: 10)
        let vConsstrait=NSLayoutConstraint(item: newView2, attribute: .leading, relatedBy: .equal, toItem: newView, attribute: .leading, multiplier: 1, constant: 10)
        let tralingConsrait=NSLayoutConstraint(item: newView2, attribute: .trailing, relatedBy: .equal, toItem: newView, attribute: .trailing, multiplier: 1, constant: -10)
        let wConstrait=NSLayoutConstraint(item: newView2, attribute: .bottom, relatedBy: .equal, toItem: newView, attribute: .bottom, multiplier: 1, constant: -10)
        
       
        NSLayoutConstraint.activate([hConstrait,vConsstrait,wConstrait,tralingConsrait])
        */
        /*
        newView2.translatesAutoresizingMaskIntoConstraints=false
        let hConstrait=NSLayoutConstraint(item: newView2, attribute: .top, relatedBy:.equal, toItem: newView, attribute: .bottom, multiplier: 1, constant: 10)
        let vConsstrait=NSLayoutConstraint(item: newView2, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 10)
        let tralingConsrait=NSLayoutConstraint(item: newView2, attribute: .trailing, relatedBy: .equal, toItem:view , attribute: .trailing, multiplier: 1, constant: -10)
        let wConstrait=NSLayoutConstraint(item: newView2, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -40)
        
        NSLayoutConstraint.activate([hConstrait,vConsstrait,wConstrait,tralingConsrait])
        */
        
        let container = UIView()
        container.backgroundColor = UIColor.red
        container.layer.cornerRadius=5.0
        container.layer.masksToBounds = true
        let ff=UIGestureRecognizer(target: self, action: #selector(self.someAction))
        container.addGestureRecognizer(ff)
        container.isUserInteractionEnabled = true
        view.addSubview(container)
        
        container.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: container, attribute: .top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: .top, multiplier: 1, constant: 100)
        
        let verticalConstraint = NSLayoutConstraint(item: container, attribute: .leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: .leading, multiplier: 1, constant: 100)
        
        /*let widthConstraint = NSLayoutConstraint(item: container, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 200)
        let heightConstraint = NSLayoutConstraint(item: container, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 200)
 */
        let widthConstraint = NSLayoutConstraint(item: container, attribute: .bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -100)
        
        let heightConstraint = NSLayoutConstraint(item: container, attribute: .trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: .trailing, multiplier: 1, constant:-100)
        
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        
       
        myview.backgroundColor = UIColor.blue
        container.addSubview(myview)
        myview.tag=1;
        
        myview.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint1 = NSLayoutConstraint(item: myview, attribute: .top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .top, multiplier: 1, constant:0 )
        let verticalConstraint1 = NSLayoutConstraint(item: myview, attribute: .leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .leading, multiplier: 1, constant: 10)
        
        let widthConstraint1 = NSLayoutConstraint(item: myview, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .width, multiplier: 1, constant: -20)
        let heightConstraint1 = NSLayoutConstraint(item: myview, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .height, multiplier: 1/3, constant: 0)
        
        NSLayoutConstraint.activate([horizontalConstraint1, verticalConstraint1, widthConstraint1, heightConstraint1])
        
        
        
        
        myview2.backgroundColor = UIColor.yellow
        container.addSubview(myview2)
        myview2.tag=2;
        
        myview2.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint2 = NSLayoutConstraint(item: myview2, attribute: .top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: myview, attribute: .bottom, multiplier: 1, constant:0 )
        let verticalConstraint2 = NSLayoutConstraint(item: myview2, attribute: .leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .leading, multiplier: 1, constant: 10)
        
        let widthConstraint2 = NSLayoutConstraint(item: myview2, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .width, multiplier: 1, constant: -20)
        let heightConstraint2 = NSLayoutConstraint(item: myview2, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .height, multiplier: 1/3, constant: 0)
        
        NSLayoutConstraint.activate([horizontalConstraint2, verticalConstraint2, widthConstraint2, heightConstraint2])
        
        
        
       
        myview3.backgroundColor = UIColor.purple
        container.addSubview(myview3)
        myview3.tag=3;
        
        myview3.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint3 = NSLayoutConstraint(item: myview3, attribute: .top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: myview2, attribute: .bottom, multiplier: 1, constant:0 )
        let verticalConstraint3 = NSLayoutConstraint(item: myview3, attribute: .leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .leading, multiplier: 1, constant: 10)
        
        let widthConstraint3 = NSLayoutConstraint(item: myview3, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .width, multiplier: 1, constant: -20)
        let heightConstraint3 = NSLayoutConstraint(item: myview3, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .height, multiplier: 1/3, constant: 0)
        
        NSLayoutConstraint.activate([horizontalConstraint3, verticalConstraint3, widthConstraint3, heightConstraint3])
        
        
        /*
        let myview = UIView()
        myview.backgroundColor = UIColor.blue
        container.addSubview(myview)
        myview.tag=1;
        
        myview.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint1 = NSLayoutConstraint(item: myview, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint1 = NSLayoutConstraint(item: myview, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint1 = NSLayoutConstraint(item: myview, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .width, multiplier: 1, constant: -10)
        let heightConstraint1 = NSLayoutConstraint(item: myview, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: container, attribute: .height, multiplier: 1, constant: -10)
        
        NSLayoutConstraint.activate([horizontalConstraint1, verticalConstraint1, widthConstraint1, heightConstraint1])
        
        for mm in container.subviews {
            if mm.tag==1
            {
                //myview.removeFromSuperview()
                
            }
        }
      
        
        /*
        let newView2 = UIView()
        newView2.backgroundColor = UIColor.blue
        view.addSubview(newView2)
        */
        
        let view2=UIView()
        view2.backgroundColor = .black
        container.addSubview(view2)
        
        view2.translatesAutoresizingMaskIntoConstraints=false
        let m1=NSLayoutConstraint(item: view2, attribute: .top, relatedBy: .equal, toItem: myview, attribute: .top, multiplier: 1, constant: 10)
        let m2=NSLayoutConstraint(item: view2, attribute: .leading, relatedBy: .equal, toItem: myview, attribute: .leading, multiplier: 1, constant: 10)
        let m3=NSLayoutConstraint(item: view2, attribute: .bottom, relatedBy: .equal, toItem: myview, attribute: .bottom, multiplier: 1, constant: -10)
        let m4=NSLayoutConstraint(item: view2, attribute: .trailing, relatedBy: .equal, toItem: myview, attribute: .trailing, multiplier: 1, constant: -10)
        NSLayoutConstraint.activate([m1,m2,m3,m4])
 
        */
        
        let btn=UIButton(type: .roundedRect)
        btn.setTitle("blue", for: .normal)
        
        btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        view.addSubview(btn)
         btn.translatesAutoresizingMaskIntoConstraints=false
        let mm1=NSLayoutConstraint(item: btn, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1, constant: 0)
       
        let mm3=NSLayoutConstraint(item: btn, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -40)
        let mm4=NSLayoutConstraint(item: btn, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
        
        let mm2=NSLayoutConstraint(item: btn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
        NSLayoutConstraint.activate([mm1,mm2,mm3,mm4])
        
       
    }
    
    @objc func someAction() {
        
         print("erhan")
    }
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        if myview.isHidden {
            myview.isHidden=false
            
        }else
        {
            myview.isHidden=true
        }
        
    }

}

